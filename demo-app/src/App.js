import React, {useState, useEffect} from 'react';
import logo from './logo.svg';
import './App.css';
import CoderInput from './CoderInput';
import Debugger from './Debugger';
import { CONSTANTS } from './constant/constant';
import axios from 'axios'

function App() {
  const [code, setCode] = useState('');
  const [results, setResults] = useState('');

  function runDebug(event) {
    event.preventDefault();
   
    const target = event.target;
    
    if (target.classList.contains(CONSTANTS.DEBUG)) {
      console.log(target.getAttribute(CONSTANTS.REL));
    }
  }

  function compile(brainFreeze) {
   setCode(brainFreeze);
   fetchCompliedCode(brainFreeze);
  }

  async function fetchCompliedCode(brainFreeze) {
    const URL = `${CONSTANTS.URL}?name=${brainFreeze}`;
    const data = await fetch(URL);
    console.log(data.data);
    setResults('results')
  }
  
  // End
  return (
    <div className="App">
      <header className="App-header">BrainF#@k Debugger</header>

      <div className="app-content">
        <CoderInput compile={compile} />
        {results && <Debugger runDebug={runDebug} results={results} />}
      </div>
    </div>
  );
}

export default App;
