export const CONSTANTS = {
  DEBUG: 'debug',
  REL: 'rel',
  URL: 'http:localhost:3001/users'
};

export const DEBUG_TOOLS = {
  START: '&#8676;',
  PREV: '&#8592;',
  NEXT: '&#8594;',
  END: '&#8677;',
  REFRESH: '&#8634;'
};

export const ITEMS = [
  {
    active: true,
    code: 'Hello wold'
  },
  {
    active: false,
    code: ' .'
  },
  {
    active: false,
    code: ' .'
  },
  {
    active: false,
    code: ' .'
  },
  {
    active: false,
    code: ' .'
  }
];