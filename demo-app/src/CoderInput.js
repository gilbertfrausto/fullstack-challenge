import React, {useState} from 'react'

export default function CoderInput({compile}) {
  
  const [code, setVal] = useState('')

  return (
    <div className="coder-input">
      <textarea onChange={event => setVal(event.target.value)} className="code-text" placeholder="Enter BrainFreeze"></textarea>
      <button className="compile-btn" onClick={() => compile(code)}>Compile</button>
    </div>
  )
}
