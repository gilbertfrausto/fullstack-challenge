import React from 'react'
import { DEBUG_TOOLS, ITEMS as items } from './constant/constant';

export default function Debugger({runDebug, results}) {
  return (
    <div className="debugger" onClick={runDebug}>
      <a href="/" className="debug"  rel="start"> &#8676;</a>
      <a href="/" className="debug" rel="prev">&#8592;</a>
      <a href="/" className="debug" rel="next"> &#8594;</a>
      <a href="/" className="debug" rel="end"> &#8677;</a>
      <a href="/" className="debug" rel="refresh"> &#8634;</a>
      
      <ul>      
        {items.map((item, index) => (
          <li key={index + 10} className={(item.active === true) ? 'active' : ' '}>
            <p key={index}>{item.code}</p>
          </li>
        ))}
      </ul>
    </div>
  )
}
